package com.galfins.gogpsextracts.Glonass;

import com.galfins.gogpsextracts.KeplerianEphemeris;

public class GlonassEphemeris extends KeplerianEphemeris {
    public final float tow;
    public final float tauN;
    public final float gammaN;
    public final double tk;

    public final double X;
    public final double Xv;
    public final double Xa;
    public final double Bn;

    public final double Y;
    public final double Yv;
    public final double Ya;
    public final int freq_num;
    public final double tb;

    public final double Z;
    public final double Zv;
    public final double Za;
    public final double En;
    private GlonassEphemeris(GlonassEphemeris.Builder builder) {
        super(builder);
        this.tow = builder.tow;
        this.tauN = builder.tauN;
        this.gammaN = builder.gammaN;
        this.tk = builder.tk;
        this.X = builder.X;
        this.Xv = builder.Xv;
        this.Xa = builder.Xa;
        this.Bn = builder.Bn;
        this.Y = builder.Y;
        this.Yv = builder.Yv;
        this.Ya = builder.Ya;
        this.freq_num = builder.freq_num;
        this.tb = builder.tb;
        this.Z = builder.Z;
        this.Zv = builder.Zv;
        this.Za = builder.Za;
        this.En = builder.En;
    }

    public static GlonassEphemeris.Builder newBuilder() {
        return new GlonassEphemeris.Builder();
    }

    public static class Builder extends KeplerianEphemeris.Builder<GlonassEphemeris.Builder> {
        private float tow;

        private float tauN;
        private float gammaN;
        private double tk;

        private double X;
        private double Xv;
        private double Xa;
        private double Bn;

        private double Y;
        private double Yv;
        private double Ya;
        private int freq_num;
        private double tb;

        private double Z;
        private double Zv;
        private double Za;
        private double En;

        private Builder() {
        }

        public GlonassEphemeris.Builder getThis() {
            return this;
        }

        public GlonassEphemeris.Builder setTow(float tow) {
            this.tow = tow;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setTauN(float tauN) {
            this.tauN = tauN;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setGammaN(float gammaN) {
            this.gammaN = gammaN;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setTk(double tk) {
            this.tk = tk;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setX(double x) {
            X = x;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setXv(double xv) {
            Xv = xv;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setXa(double xa) {
            Xa = xa;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setBn(double bn) {
            Bn = bn;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setY(double y) {
            Y = y;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setYv(double yv) {
            Yv = yv;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setYa(double ya) {
            Ya = ya;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setFreq_num(int freq_num) {
            this.freq_num = freq_num;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setTb(double tb) {
            this.tb = tb;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setZ(double z) {
            Z = z;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setZv(double zv) {
            Zv = zv;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setZa(double za) {
            Za = za;
            return this.getThis();
        }

        public GlonassEphemeris.Builder setEn(double en) {
            En = en;
            return this.getThis();
        }

        public GlonassEphemeris build() {
            return new GlonassEphemeris(this);
        }
    }
}