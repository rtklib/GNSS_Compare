package com.galfins.gogpsextracts;

public class KeplerianModel {
    public final double toeS;
    public final double deltaN;
    public final double m0;
    public final double eccentricity;
    public final double sqrtA;
    public final double omegaDot;
    public final double omega;
    public final double omega0;
    public final double iDot;
    public final double i0;
    public final double cic;
    public final double cis;
    public final double crc;
    public final double crs;
    public final double cuc;
    public final double cus;

    public KeplerianModel(KeplerianModel.Builder builder) {
        this.toeS = builder.toeS;
        this.deltaN = builder.deltaN;
        this.m0 = builder.m0;
        this.eccentricity = builder.eccentricity;
        this.sqrtA = builder.sqrtA;
        this.omegaDot = builder.omegaDot;
        this.omega = builder.omega;
        this.iDot = builder.iDot;
        this.i0 = builder.i0;
        this.omega0 = builder.omega0;
        this.cic = builder.cic;
        this.cis = builder.cis;
        this.crc = builder.crc;
        this.crs = builder.crs;
        this.cuc = builder.cuc;
        this.cus = builder.cus;
    }

    public static KeplerianModel.Builder newBuilder() {
        return new KeplerianModel.Builder();
    }

    public static class Builder {
        private double toeS;
        private double deltaN;
        private double m0;
        private double eccentricity;
        private double sqrtA;
        private double omegaDot;
        private double omega;
        private double iDot;
        private double i0;
        private double omega0;
        private double cic;
        private double cis;
        private double crc;
        private double crs;
        private double cuc;
        private double cus;

        private Builder() {
        }

        public KeplerianModel.Builder setToeS(double toeS) {
            this.toeS = toeS;
            return this;
        }

        public KeplerianModel.Builder setDeltaN(double deltaN) {
            this.deltaN = deltaN;
            return this;
        }

        public KeplerianModel.Builder setM0(double m0) {
            this.m0 = m0;
            return this;
        }

        public KeplerianModel.Builder setEccentricity(double eccentricity) {
            this.eccentricity = eccentricity;
            return this;
        }

        public KeplerianModel.Builder setSqrtA(double sqrtA) {
            this.sqrtA = sqrtA;
            return this;
        }

        public KeplerianModel.Builder setOmegaDot(double omegaDot) {
            this.omegaDot = omegaDot;
            return this;
        }

        public KeplerianModel.Builder setOmega(double omega) {
            this.omega = omega;
            return this;
        }

        public KeplerianModel.Builder setIDot(double iDot) {
            this.iDot = iDot;
            return this;
        }

        public KeplerianModel.Builder setI0(double i0) {
            this.i0 = i0;
            return this;
        }

        public KeplerianModel.Builder setOmega0(double omega0) {
            this.omega0 = omega0;
            return this;
        }

        public KeplerianModel.Builder setCic(double cic) {
            this.cic = cic;
            return this;
        }

        public KeplerianModel.Builder setCis(double cis) {
            this.cis = cis;
            return this;
        }

        public KeplerianModel.Builder setCrc(double crc) {
            this.crc = crc;
            return this;
        }

        public KeplerianModel.Builder setCrs(double crs) {
            this.crs = crs;
            return this;
        }

        public KeplerianModel.Builder setCuc(double cuc) {
            this.cuc = cuc;
            return this;
        }

        public KeplerianModel.Builder setCus(double cus) {
            this.cus = cus;
            return this;
        }

        public KeplerianModel build() {
            return new KeplerianModel(this);
        }
    }
}