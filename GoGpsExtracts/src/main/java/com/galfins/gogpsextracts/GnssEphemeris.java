package com.galfins.gogpsextracts;

public abstract class GnssEphemeris {
    public final int svid;

    protected GnssEphemeris(GnssEphemeris.Builder<?> builder) {
        this.svid = builder.svid;
    }

    public abstract static class Builder<T extends GnssEphemeris.Builder<T>> {
        private int svid;

        public Builder() {
        }

        public abstract T getThis();

        public T setSvid(int svid) {
            this.svid = svid;
            return this.getThis();
        }
    }
}
